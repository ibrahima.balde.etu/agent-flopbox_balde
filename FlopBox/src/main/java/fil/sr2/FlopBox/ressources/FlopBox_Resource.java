package fil.sr2.FlopBox.ressources;


import fil.sr2.FlopBox.model.Personne;
import fil.sr2.FlopBox.services.FlopBox_Service;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.UnknownHostException;
import java.net.http.HttpResponse;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.print.attribute.standard.Media;

import com.sun.research.ws.wadl.Application;

import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.FormParam;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.HEAD;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

@Path("/flopbox")
public class FlopBox_Resource {
	public static boolean hasConnectedInFlopBox = false;
	FlopBox_Service flopbox ;
	public static String NOM_SERVEUR;
	public static int PORT_SERVEUR;
	public static String COMPTE =  "compte.txt"; 
	
	public static String USER_FLOPBOX = "";
	
	public static int k = 1;


	

	public FlopBox_Resource() {
		this.flopbox = new FlopBox_Service();
		
	
	}
	
	/**
	 * 
	 * @param user
	 * @param pass
	 * @return
	 * @throws IOException
	 * @throws InvalidKeyException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 */
	@POST
	@Produces("text/html")
	@Path("/compte/{user}/{pass}")
	public String creerUnCompte(@PathParam("user")String user,@PathParam("pass")String pass) throws IOException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {
		if(flopbox.userFlopBoxExist(user, pass)) {
			return FlopBox_Service.HEAD+"<h3>Ce compte existe déjà,veuillez choisir un autre </h3>"+FlopBox_Service.FOOT;
		}
		String chaine = flopbox.encrypt(user+" | "+pass, pass);
		if(chaine != null) {
			this.flopbox.ajouterCompte(chaine);
			return FlopBox_Service.HEAD+"<h3>Votre compte est crée </h3>"+FlopBox_Service.FOOT;
		}
		return FlopBox_Service.HEAD+"<h3>Erreur sur la création de votre compte </h3>"+FlopBox_Service.FOOT;
	}
	
	/**
	 * 
	 * @param server
	 * @param port
	 * @param userServer
	 * @param passwordServer
	 * @param passwordFlopox
	 * @param mot
	 * @return
	 * @throws IOException
	 */
	@POST
	@Produces("text/html")
	@Path("/{nomServeur}/{portServeur}/{userServeur}/{passwordServeur}/{passwordFlopBox}/{motPourSupprimerUnServeur}")
	public String ENR(@PathParam("nomServeur") String server,@PathParam("portServeur") int port,@PathParam("userServeur") String userServer,@PathParam("passwordServeur") String passwordServer,@PathParam("passwordFlopBox") String passwordFlopox,@PathParam("motPourSupprimerUnServeur") String mot) throws IOException {
		
		if(!this.hasConnectedInFlopBox) {
			return FlopBox_Service.HEAD+"<h3>Veuillez vous connecter</h3>"+FlopBox_Service.FOOT;
		}
		
		  this.flopbox.ajoutServeur(FlopBox_Resource.USER_FLOPBOX,server,port,userServer,passwordServer,mot,passwordFlopox);
		 return FlopBox_Service.HEAD+"<h3>Serveur "+server+" ajouté avec succès</h3>"+FlopBox_Service.FOOT;
		
	}
	
	
	/**
	 * 
	 * @return
	 */
	@GET
	@Path("/serveurs")
	@Produces(MediaType.APPLICATION_JSON)
	public HashMap<String,Integer> listServers(){
		if(!FlopBox_Resource.hasConnectedInFlopBox) {
			return null;
		}
		
		HashMap<String,Integer> map = new HashMap<>();
		for (Personne p:FlopBox_Service.serveurs) {
			map.put(p.getNomServeur(), p.getPortServeur());
		}
		return map;
	}
	
	/**
	 * 
	 * @param user
	 * @param pass
	 * @return
	 * @throws IOException
	 */
	@GET
	@Path("/{userFlopBox}/{passFlopBox}")
	@Produces("text/html")
	public String ConFlopBox(@PathParam("userFlopBox") String user,@PathParam("passFlopBox") String pass) throws IOException {
		
		if(this.flopbox.authentificationFlopBox(user, pass)) {
			FlopBox_Resource.hasConnectedInFlopBox = true;
			FlopBox_Resource.USER_FLOPBOX = user;
			
			
			return FlopBox_Service.HEAD+"<h3>Connexion reussie</h3>"+FlopBox_Service.FOOT;
		}
		return FlopBox_Service.HEAD+"<h3> user or mot de passe incorrect </h3>"+FlopBox_Service.FOOT;
	}
	
	/**
	 * 
	 * @param server
	 * @param port
	 * @param user
	 * @param pass
	 * @return
	 * @throws UnknownHostException
	 * @throws IOException
	 */
	@GET
	@Path("/{nomServeur}/{portServeur}/{userFTP}/{passFTP}")
	@Produces("text/html")
	public String CON(@PathParam("nomServeur") String server,@PathParam("portServeur") int port,@PathParam("userFTP") String user,@PathParam("passFTP") String pass) throws UnknownHostException, IOException {
		
		String data;
		if(!FlopBox_Resource.hasConnectedInFlopBox) {
			return FlopBox_Service.HEAD+"<h3>Veuillez vous connecter</h3>"+FlopBox_Service.FOOT;
		}
		
		boolean reponse = this.flopbox.authentificationFTP(user, pass, server, port);
		if (reponse) {
			
			FlopBox_Resource.NOM_SERVEUR = server;
			FlopBox_Resource.PORT_SERVEUR = port;
			this.flopbox.createDirectoryDelete();
			data = this.flopbox.corps("/",server,port,user,pass);
			this.flopbox.getFtp().logout();
			return data;	 
		}
		
		return FlopBox_Service.HEAD+"<h3>connexion refusée sur le serveur, données invalides</h3>"+FlopBox_Service.FOOT;
		
	}
	
	/**
	 * 
	 * @param document
	 * @param server
	 * @param port
	 * @param user
	 * @param pass
	 * @return
	 * @throws IOException
	 */
	@GET
	@Produces("text/html")
	@Path("/repertoires/{nom_document:.*}/{nomServeur}/{portServeur}/{userFTP}/{passFTP}")
	public String CWD(@PathParam("nom_document") String document,@PathParam("nomServeur") String server,@PathParam("portServeur") int port,@PathParam("userFTP") String user,@PathParam("passFTP") String pass) throws IOException {
		if(!FlopBox_Resource.hasConnectedInFlopBox) {
			return FlopBox_Service.HEAD+"<h3>Veuillez vous connecter</h3>"+FlopBox_Service.FOOT;
		}
		boolean reponse = this.flopbox.authentificationFTP(user, pass, server, port);
		if (reponse) {
			
			try {
				this.flopbox.cd(document,server,port,user,pass);
			} catch (IOException e) {
				return this.flopbox.corps(document,server,port,user,pass)+"<h4>error: dossier incorrecte</h4>";
			}
			
			String data = this.flopbox.corps(document,server,port,user,pass);
			this.flopbox.getFtp().logout();
			return data;
		
			
		}
		return FlopBox_Service.HEAD+"<h3>connexion refusée sur le serveur, données invalides</h3>"+FlopBox_Service.FOOT;
	}
	

	/**
	 * 
	 * @param fichier
	 * @param destination
	 * @param server
	 * @param port
	 * @param user
	 * @param pass
	 * @return
	 * @throws IOException
	 */
	@POST
	@Produces("text/html")
	@Consumes(MediaType.APPLICATION_OCTET_STREAM)
	@Path("/text{fichier:.*}/-/{dest:.*}/{nomServeur}/{portServeur}/{userFTP}/{passFTP}")
	public String put_t(@PathParam("fichier") String fichier,@PathParam("dest") String destination,@PathParam("nomServeur") String server,@PathParam("portServeur") int port,@PathParam("userFTP") String user,@PathParam("passFTP") String pass) throws IOException{
		if(!FlopBox_Resource.hasConnectedInFlopBox) {
			return FlopBox_Service.HEAD+"<h3>Veuillez vous connecter</h3>"+FlopBox_Service.FOOT;
		}
		boolean reponse = this.flopbox.authentificationFTP(user, pass, server, port);
		if (reponse) {
			
			String rep = this.flopbox.PUT(fichier,destination,false);
			System.out.println(" le fichier : "+fichier );
			System.out.println("destination : "+destination);
			//System.out.println("fichier valide eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee");
			this.flopbox.getFtp().disconnect();
			return FlopBox_Service.HEAD+"<h3>Fichier sauvegardé</h3>"+FlopBox_Service.FOOT;
		}
		return FlopBox_Service.HEAD+"<h3>connexion refusée sur le serveur, données invalides</h3>"+FlopBox_Service.FOOT;
	}
	
	/**
	 * 
	 * @param fichier
	 * @param destination
	 * @param server
	 * @param port
	 * @param user
	 * @param pass
	 * @return
	 * @throws IOException
	 */
	@POST
	@Produces("text/html")
	@Consumes(MediaType.APPLICATION_OCTET_STREAM)
	@Path("/{fichier:.*}/-/{dest:.*}/{nomServeur}/{portServeur}/{userFTP}/{passFTP}")
	public String put_r(@PathParam("fichier") String fichier,@PathParam("dest") String destination,@PathParam("nomServeur") String server,@PathParam("portServeur") int port,@PathParam("userFTP") String user,@PathParam("passFTP") String pass) throws IOException{
		if(!FlopBox_Resource.hasConnectedInFlopBox) {
			return FlopBox_Service.HEAD+"<h3>Veuillez vous connecter</h3>"+FlopBox_Service.FOOT;
		}
		boolean reponse = this.flopbox.authentificationFTP(user, pass, server, port);
		if (reponse) {
			String rep = this.flopbox.PUTR("/"+fichier,destination);
			 this.flopbox.getFtp().disconnect();
			return rep;
			//return FlopBox_Service.HEAD+"<h3>Fichier sauvegardé</h3>"+FlopBox_Service.FOOT;
		}
		return FlopBox_Service.HEAD+"<h3>connexion refusée sur le serveur, données invalides</h3>"+FlopBox_Service.FOOT;
	}
	
	/**
	 * 
	 * @param fichier
	 * @param destination
	 * @param server
	 * @param port
	 * @param user
	 * @param pass
	 * @return
	 * @throws IOException
	 */
	@POST
	@Produces("text/html")
	@Consumes(MediaType.APPLICATION_OCTET_STREAM)
	@Path("/binaire{fichier:.*}/-/{dest:.*}/{nomServeur}/{portServeur}/{userFTP}/{passFTP}")
	public String put_b(@PathParam("fichier") String fichier,@PathParam("dest") String destination,@PathParam("nomServeur") String server,@PathParam("portServeur") int port,@PathParam("userFTP") String user,@PathParam("passFTP") String pass) throws IOException{
		if(!FlopBox_Resource.hasConnectedInFlopBox) {
			return FlopBox_Service.HEAD+"<h3>Veuillez vous connecter</h3>"+FlopBox_Service.FOOT;
		}
		boolean reponse = this.flopbox.authentificationFTP(user, pass, server, port);
		if (reponse) {
			String rep = this.flopbox.PUT(fichier,destination,true);
			this.flopbox.getFtp().disconnect();
			
			return FlopBox_Service.HEAD+"<h3>Fichier sauvegardé</h3>"+FlopBox_Service.FOOT;
		}
		return FlopBox_Service.HEAD+"<h3>connexion refusée sur le serveur, données invalides</h3>"+FlopBox_Service.FOOT;
	}
	
	/**
	 * 
	 * @param fichier
	 * @param server
	 * @param port
	 * @param user
	 * @param pass
	 * @return
	 */
	@POST
	@Produces("text/html")
	@Path("/dossier/{fichier:.*}/{nomServeur}/{portServeur}/{userFTP}/{passFTP}")
	public String MKD(@PathParam("fichier") String fichier,@PathParam("nomServeur") String server,@PathParam("portServeur") int port,@PathParam("userFTP") String user,@PathParam("passFTP") String pass){
		if(!FlopBox_Resource.hasConnectedInFlopBox) {
			return FlopBox_Service.HEAD+"<h3>Veuillez vous connecter</h3>"+FlopBox_Service.FOOT;
		}
		boolean reponse = this.flopbox.authentificationFTP(user, pass, server, port);
		if (reponse) {
			try {
				if (this.flopbox.MKD(fichier)) {
					this.flopbox.getFtp().disconnect();
					return "Repertoire créer";
				}
			} catch (IOException e) {
				e.printStackTrace();
				
			}
		}
		return FlopBox_Service.HEAD+"<h3>Impossible de créer le répertoire</h3>"+FlopBox_Service.FOOT;
	}
	
	/**
	 * 
	 * @param fichier
	 * @param destination
	 * @param server
	 * @param port
	 * @param user
	 * @param pass
	 */
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/text/dossier/{fichier:.*}/-/{dest:.*}/{nomServeur}/{portServeur}/{userFTP}/{passFTP}")
	public void Get_t(@PathParam("fichier") String fichier,@PathParam("dest") String destination,@PathParam("nomServeur") String server,@PathParam("portServeur") int port,@PathParam("userFTP") String user,@PathParam("passFTP") String pass){
		if(FlopBox_Resource.hasConnectedInFlopBox) {
			boolean reponse = this.flopbox.authentificationFTP(user, pass, server, port);
			if (reponse) {
				try {
					 this.flopbox.get(fichier,"/"+destination,false);
	
					
				} catch (IOException e) {
					e.printStackTrace();
				}
				//return null;
			}
		}
		//return null;
	}
	
	
	
	/**
	 * 
	 * @param fichier
	 * @param dest
	 * @param server
	 * @param port
	 * @param user
	 * @param pass
	 */
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/dossier/{src:.*}/-/{dest:.*}/{nomServeur}/{portServeur}/{userFTP}/{passFTP}")
	public void GetR(@PathParam("src") String fichier,@PathParam("dest") String dest,@PathParam("nomServeur") String server,@PathParam("portServeur") int port,@PathParam("userFTP") String user,@PathParam("passFTP") String pass){
		if(FlopBox_Resource.hasConnectedInFlopBox) {
			boolean reponse = this.flopbox.authentificationFTP(user, pass, server, port);
			if (reponse) {
				try {
					 this.flopbox.getR("/"+fichier,"/"+dest);
	
					
				} catch (IOException e) {
					e.printStackTrace();
				}
				//return null;
			}
		}
		
	}
	
	
	
	
	   
	  
	/**
	 * 
	 * @param fichier
	 * @param destination
	 * @param server
	 * @param port
	 * @param user
	 * @param pass
	 */
	@GET
	@Produces("image/jpeg")
	@Path("/binaire/dossier/{fichier:.*}/-/{dest:.*}/{nomServeur}/{portServeur}/{userFTP}/{passFTP}")
	public void Get_b(@PathParam("fichier") String fichier,@PathParam("dest") String destination,@PathParam("nomServeur") String server,@PathParam("portServeur") int port,@PathParam("userFTP") String user,@PathParam("passFTP") String pass){
		if(FlopBox_Resource.hasConnectedInFlopBox) {
			boolean reponse = this.flopbox.authentificationFTP(user, pass, server, port);
			if (reponse) {
				try {
					 this.flopbox.get(fichier,destination,true);
	
					
				} catch (IOException e) {
					e.printStackTrace();
				}
				//return null;
			}
		}
		//return null;
	}
	
	
	
	

	/**
	 * 
	 * @param server
	 * @param mot
	 * @return
	 * @throws IOException
	 */
	@DELETE
	@Produces("text/html")
	@Path("/{nomServeur}/{motPourSupprimerUnServeur}")
	public String SUP(@PathParam("nomServeur") String server,@PathParam("motPourSupprimerUnServeur") String mot) throws IOException{
	
		if(!FlopBox_Resource.hasConnectedInFlopBox) {
			return FlopBox_Service.HEAD+"<h3>Veuillez vous connecter</h3>"+FlopBox_Service.FOOT;
		}
		if(FlopBox_Service.serveurs.size() == 0) {
			return "le serveur contient pas ce nom";
		}
		int i = 0;
		boolean notFind = true ;
		while (i < FlopBox_Service.serveurs.size() && notFind) {
			if (FlopBox_Service.serveurs.get(i).getNomServeur().equals(server)){
				notFind = false;
				
			}
			i++;
		}
		i--;	
		if (!notFind) {
			if(FlopBox_Service.serveurs.get(i).getNomServeur().equals(server) && FlopBox_Service.serveurs.get(i).getMotPourSupprimerUnServeur().equals(mot)) {
				FlopBox_Service.serveurs.remove(FlopBox_Service.serveurs.get(i));
				return "serveur supprimé";
				
			}
		}
		
		
		return "le serveur contient pas ce nom";
		
		
	}
	
	/**
	 * 
	 * @param document
	 * @param server
	 * @param port
	 * @param user
	 * @param pass
	 * @return
	 * @throws UnknownHostException
	 * @throws IOException
	 */
	@GET
	@Path("/dossiers/{fichier:.*}/{nomServeur}/{portServeur}/{userFTP}/{passFTP}")
	@Produces("text/html")
	public String LIST(@PathParam("fichier") String document,@PathParam("nomServeur") String server,@PathParam("portServeur") int port,@PathParam("userFTP") String user,@PathParam("passFTP") String pass) throws UnknownHostException, IOException {
		String data;
		if(!FlopBox_Resource.hasConnectedInFlopBox) {
			return FlopBox_Service.HEAD+"<h3>Veuillez vous connecter</h3>"+FlopBox_Service.FOOT;
		}
		
		
		boolean reponse = this.flopbox.authentificationFTP(user, pass, server, port);
		if (reponse) {
			data = this.flopbox.corps(document,server,port,user,pass);
			this.flopbox.getFtp().disconnect();
			return data;	 
		}
		
		return FlopBox_Service.HEAD+"<h3>connexion refusée sur le serveur, données invalides</h3>"+FlopBox_Service.FOOT;
		
	}
	
	/**
	 * 
	 * @param nv_port_srv
	 * @param server
	 * @param port
	 * @param user
	 * @param pass
	 * @return
	 * @throws IOException
	 */
	@HEAD
	@Produces(MediaType.APPLICATION_XHTML_XML)
	@Path("/{nouveau_port_serveur}/{nomServeur}/{portServeur}/{userFTP}/{passFTP}")
	public String PORT(@PathParam("nouveau_port_serveur") int nv_port_srv,@PathParam("nomServeur") String server,@PathParam("portServeur") int port,@PathParam("userFTP") String user,@PathParam("passFTP") String pass) throws IOException {
		
		if(!this.hasConnectedInFlopBox) {
			return FlopBox_Service.HEAD+"<h3>Veuillez vous connecter</h3>"+FlopBox_Service.FOOT;
		}
		boolean reponse = this.flopbox.authentificationFTP(user, pass, server, port);
		if (reponse) {
			this.flopbox.PORT(nv_port_srv);
			return FlopBox_Service.HEAD+"<h3>nouveau port defini</h3>"+FlopBox_Service.FOOT;
		}
		return FlopBox_Service.HEAD+"<h3>connexion refusée sur le serveur, données invalides</h3>"+FlopBox_Service.FOOT;
		
	}
	
	/**
	 * 
	 * @param file
	 * @param server
	 * @param port
	 * @param user
	 * @param pass
	 * @return
	 * @throws IOException
	 */
	@DELETE
	@Produces("text/html")
	@Path("/dossier/{fichier:.*}/{nomServeur}/{portServeur}/{userFTP}/{passFTP}")
	public String RMD(@PathParam("fichier") String file,@PathParam("nomServeur") String server,@PathParam("portServeur") int port,@PathParam("userFTP") String user,@PathParam("passFTP") String pass) throws IOException {
		
		if(!this.hasConnectedInFlopBox) {
			return FlopBox_Service.HEAD+"<h3>Veuillez vous connecter</h3>"+FlopBox_Service.FOOT;
		}
		boolean reponse = this.flopbox.authentificationFTP(user, pass, server, port);
		if (reponse) {
			 try {
				return this.flopbox.RMD(file);
			} catch (Exception e) {
				
				e.printStackTrace();
			}
			
			//return this.flopbox.corps(file, server, port, user, pass);
		}
		return FlopBox_Service.HEAD+"<h3>connexion refusée sur le serveur, données invalides</h3>"+FlopBox_Service.FOOT;
		
	}
	
	/**
	 * 
	 * @param file
	 * @param server
	 * @param port
	 * @param user
	 * @param pass
	 * @return
	 * @throws IOException
	 */
	@DELETE
	@Produces("text/html")
	@Path("/file/{fichier:.*}/{nomServeur}/{portServeur}/{userFTP}/{passFTP}")
	public String RMF(@PathParam("fichier") String file,@PathParam("nomServeur") String server,@PathParam("portServeur") int port,@PathParam("userFTP") String user,@PathParam("passFTP") String pass) throws IOException {
		
		if(!this.hasConnectedInFlopBox) {
			return FlopBox_Service.HEAD+"<h3>Veuillez vous connecter</h3>"+FlopBox_Service.FOOT;
		}
		boolean reponse = this.flopbox.authentificationFTP(user, pass, server, port);
		if (reponse) {
			 try {
				return this.flopbox.RMF(file);
			} catch (Exception e) {
				
				e.printStackTrace();
			}
			
			//return this.flopbox.corps(file, server, port, user, pass);
		}
		return FlopBox_Service.HEAD+"<h3>connexion refusée sur le serveur, données invalides</h3>"+FlopBox_Service.FOOT;
		
	}
	
	/**
	 * 
	 * @param fileFr
	 * @param fileTo
	 * @param server
	 * @param port
	 * @param user
	 * @param pass
	 * @return
	 * @throws IOException
	 */
	@PUT
	@Produces("text/html")
	@Path("/dossier/{oldFile:.*}/-/{newFile:.*}/{nomServeur}/{portServeur}/{userFTP}/{passFTP}")
	public String RN_D_F(@PathParam("oldFile") String fileFr,@PathParam("newFile") String fileTo,@PathParam("nomServeur") String server,@PathParam("portServeur") int port,@PathParam("userFTP") String user,@PathParam("passFTP") String pass) throws IOException {
		
		if(!this.hasConnectedInFlopBox) {
			return FlopBox_Service.HEAD+"<h3>Veuillez vous connecter</h3>"+FlopBox_Service.FOOT;
		}
		boolean reponse = this.flopbox.authentificationFTP(user, pass, server, port);
		if (reponse) {
			this.flopbox.RN_D(fileFr,fileTo);
			return this.flopbox.corps(fileFr, server, port, user, pass);
		}
		return FlopBox_Service.HEAD+"<h3>connexion refusée sur le serveur, données invalides</h3>"+FlopBox_Service.FOOT;
		
	}
	
	/**
	 * 
	 * @param fileFr
	 * @param fileTo
	 * @param server
	 * @param port
	 * @param user
	 * @param pass
	 * @return
	 * @throws IOException
	 */
	@PUT
	@Produces("text/html")
	@Path("/file/{oldFile:.*}/-/{newFile:.*}/{nomServeur}/{portServeur}/{userFTP}/{passFTP}")
	public String RN_F(@PathParam("oldFile") String fileFr,@PathParam("newFile") String fileTo,@PathParam("nomServeur") String server,@PathParam("portServeur") int port,@PathParam("userFTP") String user,@PathParam("passFTP") String pass) throws IOException {
		
		if(!this.hasConnectedInFlopBox) {
			return FlopBox_Service.HEAD+"<h3>Veuillez vous connecter</h3>"+FlopBox_Service.FOOT;
		}
		boolean reponse = this.flopbox.authentificationFTP(user, pass, server, port);
		if (reponse) {
			this.flopbox.RN_F(fileFr,fileTo);
			return this.flopbox.corps(fileFr, server, port, user, pass);
		}
		return FlopBox_Service.HEAD+"<h3>connexion refusée sur le serveur, données invalides</h3>"+FlopBox_Service.FOOT;
		
	}
	
	/**
	 * 
	 * @param fileFr
	 * @param fileTo
	 * @param server
	 * @param port
	 * @param user
	 * @param pass
	 * @return
	 * @throws IOException
	 */
	@HEAD
	@Produces("text/html")
	@Path("/{nomServeur}/{portServeur}/{userFTP}/{passFTP}")
	public String CLOS(@PathParam("oldFile") String fileFr,@PathParam("newFile") String fileTo,@PathParam("nomServeur") String server,@PathParam("portServeur") int port,@PathParam("userFTP") String user,@PathParam("passFTP") String pass) throws IOException {
		
		if(!this.hasConnectedInFlopBox) {
			return FlopBox_Service.HEAD+"<h3>Veuillez vous connecter</h3>"+FlopBox_Service.FOOT;
		}
		boolean reponse = this.flopbox.authentificationFTP(user, pass, server, port);
		if (reponse) {
			this.flopbox.CLOS();
		}
		return FlopBox_Service.HEAD+"<h3>connexion refusée sur le serveur, données invalides</h3>"+FlopBox_Service.FOOT;
		
	}
	
	
	/**
	 * 
	 * @param user
	 * @param pass
	 * @param destination
	 * @return
	 * @throws IOException
	 */
	@GET
	@Produces("text/html")
	//@Produces(MediaType.APPLICATION_OCTET_STREAM)
	@Path("/fichierConfiguration/{userFlopBox}/{passFlopBox}/{dest:.*}")
	public String fichierConf(@PathParam("userFlopBox") String user,@PathParam("passFlopBox") String pass,@PathParam("dest") String destination) throws IOException{
		
		if(this.flopbox.authentificationFlopBox(user, pass)) {
			if(this.flopbox.envoyerFichierConf(user,pass,destination)) 
				return FlopBox_Service.HEAD+"<h3>Fichier envoyer</h3>"+FlopBox_Service.FOOT;
			return FlopBox_Service.HEAD+"<h3>Chemin du fichier incorrect</h3>"+FlopBox_Service.FOOT;	
		}
		
		return FlopBox_Service.HEAD+"<h3>Login incorrect</h3>"+FlopBox_Service.FOOT;
	}
		

	/**
	 * 
	 * @param local
	 * @param server
	 * @param port
	 * @param user
	 * @param pass
	 * @return
	 * @throws IOException
	 * @throws ParseException
	 */
	@GET
	@Produces("text/html")
	@Path("/mise_a_jour/{cheminLocal:.*}/{nomServeur}/{portServeur}/{userFTP}/{passFTP}")
	public String update_locale(@PathParam("cheminLocal") String local,@PathParam("nomServeur") String server,@PathParam("portServeur") int port,@PathParam("userFTP") String user,@PathParam("passFTP") String pass) throws IOException, ParseException{
		//System.out.println(FlopBox_Resource.k++);
		if(!FlopBox_Resource.hasConnectedInFlopBox) {
			return FlopBox_Service.HEAD+"<h3>Veuillez vous connecter</h3>"+FlopBox_Service.FOOT;
		}
		boolean reponse = this.flopbox.authentificationFTP(user, pass, server, port);
		if (reponse) {
			this.flopbox.update_locale(local,"");
		
			this.flopbox.getFtp().disconnect();
			return FlopBox_Service.HEAD+"<h3>locale has updated</h3>"+FlopBox_Service.FOOT;
		}
		return FlopBox_Service.HEAD+"<h3>connexion refusée sur le serveur, données invalides</h3>"+FlopBox_Service.FOOT;
	}
	
	

	/**
	 * 
	 * @param file
	 * @param server
	 * @param port
	 * @param user
	 * @param pass
	 * @return
	 * @throws IOException
	 * @throws ParseException
	 */
	@POST
	@Produces("text/html")
	@Path("/deleted_directory/{file:.*}/{nomServeur}/{portServeur}/{userFTP}/{passFTP}")
	public String move_To_deleted_directory(@PathParam("file") String file,@PathParam("nomServeur") String server,@PathParam("portServeur") int port,@PathParam("userFTP") String user,@PathParam("passFTP") String pass) throws IOException, ParseException {
		
		if(!FlopBox_Resource.hasConnectedInFlopBox) {
			return FlopBox_Service.HEAD+"<h3>Veuillez vous connecter</h3>"+FlopBox_Service.FOOT;
		}
		boolean reponse = this.flopbox.authentificationFTP(user, pass, server, port);
		if (reponse) {
			
			//System.out.println(file+" "+FlopBox_Resource.k);
			this.flopbox.move(file);
			FlopBox_Resource.k++;
			
			this.flopbox.getFtp().disconnect();
			return FlopBox_Service.HEAD+"<h3>locale has updated</h3>"+FlopBox_Service.FOOT;
		}
		return FlopBox_Service.HEAD+"<h3>connexion refusée sur le serveur, données invalides</h3>"+FlopBox_Service.FOOT;
		
	}
	
	/**
	 * 
	 * @param server
	 * @param port
	 * @param user
	 * @param pass
	 * @return
	 * @throws IOException
	 * @throws ParseException
	 */
	@GET
	@Produces("text/html")
	@Path("/files/{nomServeur}/{portServeur}/{userFTP}/{passFTP}")
	public String showDeletedFiles(@PathParam("nomServeur") String server,@PathParam("portServeur") int port,@PathParam("userFTP") String user,@PathParam("passFTP") String pass) throws IOException, ParseException {
		if(!FlopBox_Resource.hasConnectedInFlopBox) {
			return FlopBox_Service.HEAD+"<h3>Veuillez vous connecter</h3>"+FlopBox_Service.FOOT;
		}
		boolean reponse = this.flopbox.authentificationFTP(user, pass, server, port);
		if (reponse) {
		   
			String rep = this.flopbox.showDeletedFiles();
			
			this.flopbox.getFtp().disconnect();
			return rep;
			
		}
		return FlopBox_Service.HEAD+"<h3>connexion refusée sur le serveur, données invalides</h3>"+FlopBox_Service.FOOT;
		
	}
	
	/**
	 * 
	 * @param path
	 * @param server
	 * @param port
	 * @param user
	 * @param pass
	 * @return
	 * @throws IOException
	 * @throws ParseException
	 */
	@PUT
	@Produces("text/html")
	@Path("/document/{file:.*}/{nomServeur}/{portServeur}/{userFTP}/{passFTP}")
	public String recupUnDossierSuprime(@PathParam("file") String path,@PathParam("nomServeur") String server,@PathParam("portServeur") int port,@PathParam("userFTP") String user,@PathParam("passFTP") String pass) throws IOException, ParseException {
		if(!FlopBox_Resource.hasConnectedInFlopBox) {
			return FlopBox_Service.HEAD+"<h3>Veuillez vous connecter</h3>"+FlopBox_Service.FOOT;
		}
		boolean reponse = this.flopbox.authentificationFTP(user, pass, server, port);
		if (reponse) {
		   
			String rep = this.flopbox.recupUnDossierSuprime(path);
			this.flopbox.getFtp().disconnect();
			return rep;
			
		}
		return FlopBox_Service.HEAD+"<h3>connexion refusée sur le serveur, données invalides</h3>"+FlopBox_Service.FOOT;
		
	}
	/**
	 * 
	 * @param path
	 * @param server
	 * @param port
	 * @param user
	 * @param pass
	 * @return
	 * @throws IOException
	 * @throws ParseException
	 */
	@DELETE
	@Produces("text/html")
	@Path("/document/{file:.*}/{nomServeur}/{portServeur}/{userFTP}/{passFTP}")
	public String suprimerDefinitifUnDossierSuprime(@PathParam("file") String path,@PathParam("nomServeur") String server,@PathParam("portServeur") int port,@PathParam("userFTP") String user,@PathParam("passFTP") String pass) throws IOException, ParseException {
		if(!FlopBox_Resource.hasConnectedInFlopBox) {
			return FlopBox_Service.HEAD+"<h3>Veuillez vous connecter</h3>"+FlopBox_Service.FOOT;
		}
		boolean reponse = this.flopbox.authentificationFTP(user, pass, server, port);
		if (reponse) {
		   
			String rep = this.flopbox.suprimerDefinitifUnDossierSuprime(path);
			this.flopbox.getFtp().disconnect();
			return rep;
			
		}
		return FlopBox_Service.HEAD+"<h3>connexion refusée sur le serveur, données invalides</h3>"+FlopBox_Service.FOOT;
		
	}
	
	

}
