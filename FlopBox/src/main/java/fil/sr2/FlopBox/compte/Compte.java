package fil.sr2.FlopBox.compte;

public class Compte {
	private String user;
	private String password;
	
	public Compte(String user_name,String user_password) {
		this.user = user_name;
		this.password = user_password;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	
}
