package fil.sr2.FlopBox.serveur;


public class Serveur {
	private String motPourSupprimerUnServeur;
	private String nomServeur;
	private int portServeur;
	private String userNameServer;
	private String passWordServer;
	
	
	public Serveur(String mot,String serveur,int port,String user_name,String passWord) {
		this.motPourSupprimerUnServeur = mot;
		this.nomServeur = serveur;
		this.portServeur = port;
		this.userNameServer = user_name;
		this.passWordServer = passWord;
		
	}


	public String getMotPourSupprimerUnServeur() {
		return motPourSupprimerUnServeur;
	}


	public void setMotPourSupprimerUnServeur(String motPourSupprimerUnServeur) {
		this.motPourSupprimerUnServeur = motPourSupprimerUnServeur;
	}


	public String getNomServeur() {
		return nomServeur;
	}


	public void setNomServeur(String nomServeur) {
		this.nomServeur = nomServeur;
	}


	public int getPortServeur() {
		return portServeur;
	}


	public void setPortServeur(int portServeur) {
		this.portServeur = portServeur;
	}


	public String getUserNameServer() {
		return userNameServer;
	}


	public void setUserNameServer(String userNameServer) {
		this.userNameServer = userNameServer;
	}


	public String getPassWordServer() {
		return passWordServer;
	}


	public void setPassWordServer(String passWordServer) {
		this.passWordServer = passWordServer;
	}
	
	
	

}
