package fil.sr2.FlopBox.services;

public class InoeudFichier {
	
public String getInoeud() {
		return inoeud;
	}

	public void setInoeud(String inoeud) {
		this.inoeud = inoeud;
	}

private String inoeud;
	
	public InoeudFichier(String inoeud_fichier) {
		this.inoeud = inoeud_fichier;
	}
	
	/**@return the hashCode of the client's name
	    */
	public int hashCode(){
		return this.inoeud.hashCode();
    }
		
		/**
		 * this InoeudFichier is equals to another if they have same inoeud
		 * 
		 * 
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		
	public boolean equals(Object o) {
		if (o instanceof InoeudFichier) {
			InoeudFichier theOther = ((InoeudFichier) o);
				return this.inoeud.equals(theOther.inoeud);
			}
			return false;
	}

	

}
