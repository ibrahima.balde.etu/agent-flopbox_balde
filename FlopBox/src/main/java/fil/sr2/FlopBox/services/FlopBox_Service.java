package fil.sr2.FlopBox.services;
import fil.sr2.FlopBox.model.FTPUtil;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.commons.net.ftp.FTPSClient;
import org.glassfish.jersey.message.internal.StringBuilderUtils;

import fil.sr2.FlopBox.model.Personne;
import fil.sr2.FlopBox.ressources.FlopBox_Resource;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Struct;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.FileNotFoundException;



/**
 * @author Thierno Amadou BAH
 *
 */
public class FlopBox_Service {
	public static List<Personne> serveurs = new ArrayList<>();
	public static final String URL = "/flopbox/";
	public static final String HEAD="<html><head><meta charset=\"utf-8\" /><title>FTP Server</title></head><body>";
	public static  String TEXT = "";
	public static final String FOOT="</body>";
	public static final String FTP = "<h1>FTP</h1>";
	public String serverName="";
	private String cheminRepertoirCourant;
	public static  Map<InoeudFichier,String> inoeudFichier = new HashMap<>();
	public static  Map<String,String> lastModification = new HashMap<>();
	public static List<String> nomReperoire = new ArrayList<>();
	public static List<String> nomFic = new ArrayList<>();
	public static int START = 1;
	
	
	
	
	private FTPClient ftp;
	private Socket ds;
	
	/**
	 * Create a Flop Box service
	 *
	 */
	
	public FlopBox_Service() {
		this.ftp=new FTPClient();
		
		//this.inoeudFichier = new HashMap<>();
		//this.lastModification = new HashMap<>();
		//this.nomReperoire = new ArrayList<>();
		
		
		
	}
	
	/**
	 * @return the ftp
	 *
	 */
	public FTPClient getFtp() {
		return this.ftp;
	}
	
	/**
	 * @return all servers which are registered
	 *
	 */
	public List<Personne> listAllServers(){
		
		return FlopBox_Service.serveurs; 
	}
	
	/**
	 * @param user
	 * @param pass
	 * @return true if the user and password of flop box are right 
	 * @throws IOException 
	 */

	public boolean authentificationFlopBox(String user, String pass) throws IOException {
		/*if(!user.equals("anonymous"))return false;
		else if(!pass.equals("anonymous"))return false;
		return true;*/
		
		BufferedReader lecteurAvecBuffer = null;
	    String ligne;
		lecteurAvecBuffer = new BufferedReader(new FileReader(FlopBox_Resource.COMPTE));
		while ((ligne = lecteurAvecBuffer.readLine()) != null) {
			String chaine = decrypt(ligne,pass);
			if (chaine != null) {
				String[] reponse =  chaine.split(" | ");
				//System.out.println(reponse[0]+" "+reponse[2]);
				if(reponse[0].equals(user) && reponse[2].equals(pass)) {
					lecteurAvecBuffer.close();
					return true;
					}
			}
			
		
		}
		lecteurAvecBuffer.close();
		return false;
		
	}
	
	/**
	 * @param user
	 * @param pass
	 * @param port
	 * @return true if the user and password of the server ftp are right 
	 */
	public boolean authentificationFTP(String user, String pass,String server, int port) {
		this.serverName = server;
		try {
			ftp.connect(server,port);
			ftp.login(user,pass);
			int reply = ftp.getReplyCode();

		    if(FTPReply.isPositiveCompletion(reply)) {
		 
		        return true;
		    	
		    }
		    return false;
		} catch (IOException e) {
			e.printStackTrace();
			
		}
		return false;
	}
	
	
	/** Execute the command pwd
	 * @return urent directory
	 */
	public String pwd() {
		try {
			this.ftp.pwd();
			return this.ftp.getReplyString();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		return null;
	}
	
	
	
	public void cdup() throws IOException{
			this.ftp.cdup();
		
	}
	
	/**
	 * execute the commande cd
	 * @param user
	 * @param pass
	 * @return
	 * @throws IOException
	 */
	public String cd(String directory,String server,int port,String user,String pass) throws IOException{
		this.ftp.cwd(directory);
		
		return this.corps(directory,server,port,user,pass);
	}
	
	/*
	 * list the directory
	 */
	public String ls() {
		try {
			this.ftp.enterLocalPassiveMode();
			this.ftp.pasv();
			String passiveChaine = this.ftp.getReplyString();
			int port = FlopBox_Service.parsePassiveModeToPort(passiveChaine);
			//int portPassive = this.ftp.getPassivePort();
			this.ds =new Socket(this.ftp.getRemoteAddress(), port);
			
			this.ftp.list();
			//FTPFile[] files = this.ftp.listDirectories();
			
			
			BufferedReader reader = new BufferedReader(new InputStreamReader(this.ds.getInputStream()));
			
			String ret ="";
			
			String tmp = reader.readLine();
			
			while(tmp != null){
				ret += tmp+",";
				tmp = reader.readLine();
			}
			ftp.completePendingCommand();
			return ret;
			//this.ds.close();*/
			
		} catch (IOException e) {
			//TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}

	/**
	 * 
	 * @param file
	 * @param serveur
	 * @param port
	 * @param nom
	 * @param pass
	 * @return
	 * @throws IOException
	 */
	public String corps(String file,String serveur,int port,String nom,String pass) throws IOException {
		
		String head=FlopBox_Service.HEAD+FlopBox_Service.FTP;
		head+="<table>";
		head+="<ul>";
		String corps="";
		
		this.ftp.changeWorkingDirectory(file);
		String chemin="";

		for(String nom_dossier : this.ls().split(",")){
			String droitDeAcces = nom_dossier.split(" ")[0];
			StringBuilder input1 = new StringBuilder();
			input1.append(nom_dossier);
			  input1.reverse();
			  String []s = input1.toString().split(" ");
			  input1 = new StringBuilder(); 
			  input1.append(s[0]);
			  input1.reverse();
			  String document = input1.toString();
			  this.ftp.pwd();
			  chemin = this.ftp.getReplyString().split(" ")[1];
			  chemin.replace("\"", "");
			  if (!chemin.isEmpty()) chemin = chemin.substring(1,chemin.length()-1);
			  if(FlopBox_Service.isADirectory(nom_dossier)) {
				  
				  //if (chemin.equals("\"/\"")) chemin = "";
				  if(chemin.equals("/"))  corps +=  "<li> "+droitDeAcces+" | "+document+"</li>";
				  else
					  corps +=  "<li> "+droitDeAcces+" | "+document+"</li>";
				
			  }
			  else {
				 // corps +=  "<li> "+droitDeAcces+" | "+"<a href=\""+FlopBox_Service.URL+"get"+chemin+"/"+document+"/"+serveur+"/"+port+"/"+nom+"/"+pass+"\">"+document+"</a></li>";
				  corps +=  "<li> "+droitDeAcces+" | "+document+"</li>";
			  }
			  
		}
		if(corps.equals("")){
			corps += "<li>Dossier vide</li>";
		}
		
		return head+corps+FlopBox_Service.FOOT;
	}



/**
 * @param passiveChaine the answer after we have send a passive commande to the server
 * @return ip content in the passive message

 */
public static String parsePassiveModeToIP(String passiveChaine) {
	  String []chaine = passiveChaine.substring(passiveChaine.indexOf("(")).split(",");
	  return chaine[0].substring(1)+"."+chaine[1]+"."+chaine[2]+"."+chaine[3];
	  
}

/**
 * @param passiveChaine the answer after we have send a passive commande to the server
 * @return port content in the passive message

 */
public static int parsePassiveModeToPort(String passiveChaine) {
	
	  String []chaine = passiveChaine.substring(passiveChaine.indexOf("(")).split(",");
	  //return chaine[5].trim().substring(0,chaine[5].trim().length()-2);
	  return Integer.parseInt(chaine[4])*256+Integer.parseInt(chaine[5].trim().substring(0,chaine[5].trim().length()-2));
	  
}

/**
 * @param content
 * @return true if the content it's a directory

 */
public static boolean isADirectory(String content) {
	  String c =content.substring(0,1);
	  return c.equals("d");
}



/**
* execute the commande getR
* @param src
* @param dest
* @return
* @throws IOException
*/
public void getR(String src,String dest) throws IOException {
	File localDir = new File(src);
	this.ftp.pwd();
	String name2 = this.ftp.getReplyString().split(" ")[1];
	String name = localDir.getName();
	this.createDirectoryInLocal(dest+src);
	this.ftp.cwd(src);
	this.ls();
	for (FTPFile ftp : this.ftp.listFiles(src)) {
		if(!ftp.getName().equals("..") && !ftp.getName().equals(".")) {
			if (ftp.isFile()) {
				if (ftp.getName().contains(".")) {
					if(! ftp.getName().split("\\.")[1].trim().equals("iso")) {
						//System.out.println(dest+" **********");
						this.get(src+"/"+ftp.getName(),dest,false);
					}
				}		
			}
			else {
				
				this.pwd();
				String chemin = this.ftp.getReplyString().split(" ")[1];
				chemin = chemin.replace("\"", "");
				if(chemin.length() == 1 && chemin.charAt(0) ==('/')) chemin = "";
				else {
					if(chemin.charAt(chemin.length()-1) == '/') {
						chemin = chemin.substring(0,chemin.length()-1);
					}
				}
				//this.ftp.cwd("/"+chemin+ftp.getName());
				this.ftp.cwd(src+"/"+ftp.getName());
				String rep = this.ftp.getReplyString();
				//this.getR(src+"/"+chemin+ftp.getName(), dest+"/"+name);
				this.getR(src+"/"+ftp.getName(), dest);
				
				this.ftp.cwd("..");
			}	
		}	
	}   
	
	

}

/**
 * execute the commande get
 * @param filename
 * @param isBinary
 * @return
 * @throws IOException
 */
public void get(String filename,String dest,boolean isBinary) throws IOException {
	try {
		if(isBinary) this.ftp.setFileType(FTPClient.ASCII_FILE_TYPE);//this.ftp.setFileTransferMode(2);
		else this.ftp.setFileType(FTPClient.BINARY_FILE_TYPE);//this.ftp.setFileTransferMode(10);
		
		this.ftp.enterLocalPassiveMode();
		this.ftp.pasv();
		String passiveChaine = this.ftp.getReplyString();
		int port = FlopBox_Service.parsePassiveModeToPort(passiveChaine);
		
		this.ds =new Socket(this.ftp.getRemoteAddress(), port);
		
		//System.out.println(filename+" fff");
		/*FileOutputStream fileStream = null;
		InputStream in = null;
		//if (filename.charAt(0) == '/') filename = filename.substring(1,filename.length());
		fileStream = new FileOutputStream(filename);
		
		//System.out.println(dest+"/"+filename+" --------");
		this.ftp.retrieveFile(dest+"/"+filename, fileStream);
		fileStream.close();*/
		
		String remoteFile1 = dest;
		//+"/"+filename
        File downloadFile1 = new File(dest+"/");
		System.out.println("destination et filename : "+dest);
		System.out.println("filename : "+ filename);
		System.out.println(" downloadFile1 et filename : "+downloadFile1+filename);
        OutputStream outputStream1 = new BufferedOutputStream(new FileOutputStream(downloadFile1+filename));

        boolean success = this.ftp.retrieveFile(remoteFile1, outputStream1);
		System.out.println("Response ftp : "+ this.ftp.getReplyString());
        outputStream1.close();
		
		
	} catch (IOException e) {
		e.printStackTrace();
	}
	
}







/**
 * execute the commande PUT
 * @param directory
 * @param dest
 * @throws IOException 
 */
public String PUTR(String directory,String dest) throws IOException {
	
	
	
    File localDir = new File(directory);
    File[] subFiles = localDir.listFiles();
    this.ftp.cwd(dest);
    this.ftp.pwd();
    
	String chemin = this.ftp.getReplyString().split(" ")[1];
	
	chemin = chemin.replace("\"", "");
	if(chemin.length() == 1 && chemin.charAt(0) ==('/')) chemin = "";
	else {
		if(chemin.charAt(chemin.length()-1) == '/') {
			chemin = chemin.substring(0,chemin.length()-1);
		}
	}
	String name = localDir.getName();
	this.MKD(name);
    this.ftp.cwd(name);
    if (subFiles != null && subFiles.length > 0) {
        for (File item : subFiles) {
        	if(!item.getName().equals("..") && !item.getName().equals(".")) {
        		if (item.isFile()) {
        			File fileTmp = new File(directory+"/"+item.getName());
        			
	                this.PUT(directory+"/"+item.getName(),dest+"/"+name, false);
	             } else {
	            	// System.out.println(directory+"/"+item.getName());
	            	 //this.MKD(name);
	            	 this.PUTR(directory+"/"+item.getName(),dest+"/"+name);
	            	 this.ftp.cwd("..");
	            }
        	}
        }
    }
    this.enregister_nom_fic(directory);
    return this.ftp.getReplyString();
   
}

/**
 * execute the commande PUT
 * @param file
 * @param dest
 * @param isBinary
 */
public String PUT(String file,String dest,boolean isBinary){
	//System.out.println("Bonjour la destiantion");
	try {
		System.out.println("file : "+file);
		this.ftp.enterLocalPassiveMode();
		if(isBinary) this.ftp.setFileType(FTPClient.BINARY_FILE_TYPE);
		else this.ftp.setFileType(FTPClient.ASCII_FILE_TYPE);
		this.ftp.pasv();
		if (file.charAt(file.length()-1) == '/') {
			file.substring(file.length()-1);
		}
		String[] f  = file.split("/");
		
		int n = f.length;
		String fileName = f[n-1];
		System.out.println(" Nom du fichier : "+fileName );
		InputStream file2 = new FileInputStream(file);
		
		String passiveChaine = this.ftp.getReplyString();
		int port = FlopBox_Service.parsePassiveModeToPort(passiveChaine);
		this.ds = new Socket(this.ftp.getRemoteAddress(), port);
		//System.out.println(dest);
		System.out.println("file 2 : "+file2);
		this.ftp.storeFile("/"+dest+"/"+fileName, file2);
		System.out.println("/"+dest+"/"+fileName);
		System.out.println("Reply string : "+this.ftp.getReplyString());
		return this.ftp.getReplyString();
		//return passiveChaine;

		
	} catch (IOException e) {
		e.printStackTrace();
	}
	return null;
}




/**
 * execute the command MKD
 * @param file
 * @return true if the directory has created
 * @throws IOException
 */
public boolean MKD(String file) throws IOException{
	
     return this.ftp.makeDirectory(file);
}

/**
 * @param port
 * @throws IOException
 */
public String PORT(int port) throws IOException{
	
	this.ftp.port(this.ftp.getRemoteAddress(),port);
	return this.ftp.getReplyString();
	
}

/**
 * execute the command RMD
 * @param directory
 * @throws IOException 
 */
public String RMD(String directory) throws IOException{
	
	FTPFile[] dirs = null;
	try {
		dirs = this.ftp.listFiles(directory);
	}catch (IOException e) {
		e.printStackTrace();
	}
	String name,childPath;
	for(FTPFile ftpFile : dirs) {
		name = ftpFile.getName();
		childPath = directory+"/"+name;
		if(ftpFile.isDirectory()) {
			if(!name.equals(".") && !name.equals("..")) {
				if(ftpFile.getSize() ==  0){
					this.ftp.removeDirectory(childPath);
				}
				else {
					this.RMD(childPath);
				}
				
			}
		}
		else {
			this.RMF(childPath);
		}
		
	}
	this.ftp.removeDirectory(directory);
	return this.ftp.getReplyString();
	
}






/**
 * to delete a file
 * @param fichier
 * @throws IOException
 */
public String RMF(String fichier) throws IOException{
	
	
	 this.ftp.deleteFile(fichier);
	
	return this.ftp.getReplyString();
	
}

/**
 * execute the commande RND 
 * @param oldFile
 * @param newfile
 * @return
 * @throws IOException
 */
public void RN_D(String oldFile,String newfile) throws IOException{
	
	this.ftp.rnfr(oldFile);
	this.ftp.rnto(newfile);
	return;
	
}

/**
 * execute the commande RNF
 * @param oldFile
 * @param newfile
 * @return
 * @throws IOException
 */
public String RN_F(String oldFile,String newfile) throws IOException{
	
	this.ftp.rename(oldFile, newfile);
	return this.ftp.getReplyString();
	
}

/*
 * connexion en mode passif
 */
public String PASSIF() {
	this.ftp.enterLocalPassiveMode();
	return this.ftp.getReplyString();
}

/*
 * connexion en mode actif
 */
public String ACTIF() {
	this.ftp.enterLocalActiveMode();
	return this.ftp.getReplyString();
}

/*
 * fermer la connexion ftp
 * @throws IOException
 */
public void CLOS() throws IOException {
	if(this.ftp.isConnected()) {
		this.ftp.disconnect();
	}
}

/**
 * 
 * @param chemin
 * @return
 */
public boolean createDirectoryInLocal(String chemin) {
	File dossier = new File(chemin);
	if (dossier.mkdir()) {
		return true;
	}
	return false;
}

/**
 * 
 * @param userName
 * @param password
 * @return
 * @throws IOException
 */
public boolean userFlopBoxExist(String userName,String password) throws IOException {
	
	BufferedReader lecteurAvecBuffer = null;
    String ligne;
	lecteurAvecBuffer = new BufferedReader(new FileReader(FlopBox_Resource.COMPTE));
	while ((ligne = lecteurAvecBuffer.readLine()) != null) {
		String chaine = decrypt(ligne,password);
		if (chaine != null) {
			if(chaine.split(" | ")[0].equals(userName)) { 
				lecteurAvecBuffer.close();
				return true;
			}
		}
		
	
	}
	lecteurAvecBuffer.close();
	return false;
}

/**
 * 
 * @param chaine
 * @param key
 * @return
 */
public String encrypt(String chaine,String key) {

	
	try  {   
		   MessageDigest sha =  MessageDigest.getInstance("SHA-1");
		   byte[] key2  = key.getBytes("UTF-8");
		   key2 = sha.digest(key2);
           key2 = Arrays.copyOf(key2, 16); 
           SecretKeySpec secretKey = new SecretKeySpec(key2, "AES");
           
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
           
            return  Base64.getEncoder().encodeToString(cipher.doFinal(chaine.getBytes("UTF-8")));
           } 
           catch (Exception e) 
           {
              
               return null;
           }
          
		 
}

/**
 * 
 * @param chaine
 * @throws IOException
 */
public void ajouterCompte(String chaine) throws IOException {
	File file = new File(FlopBox_Resource.COMPTE);
	FileWriter fw = new FileWriter(file.getAbsoluteFile(),true);
	BufferedWriter bw = new BufferedWriter(fw);
	bw.write(chaine);
    bw.newLine();
	bw.close();
}

/**
 * 
 * @param chaine
 * @param key
 * @return
 */
public String decrypt(String chaine,String key){
	try
	{
		MessageDigest sha =  MessageDigest.getInstance("SHA-1");
		byte[] key2  = key.getBytes("UTF-8");
		key2 = sha.digest(key2);
        key2 = Arrays.copyOf(key2, 16); 
        SecretKeySpec secretKey = new SecretKeySpec(key2, "AES");
		
		
		 Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
         cipher.init(Cipher.DECRYPT_MODE, secretKey);
         return new String(cipher.doFinal(Base64.getDecoder().decode(chaine)));
	}
	catch (Exception e)
	{
		System.out.println(e);
		return null;
	}
}

/**
 * 
 * @param user
 * @param server
 * @param port
 * @param userServer
 * @param passwordServer
 * @param mot
 * @param cle
 * @throws IOException
 */
public void ajoutServeur(String user, String server, int port, String userServer, String passwordServer, String mot,String cle) throws IOException {
	//serveursFTP.txt
	 File file = new File("serveursFTP.txt");
	 FileWriter fw = new FileWriter(file.getAbsoluteFile(),true);
	 BufferedWriter bw = new BufferedWriter(fw);
	 String chaine = this.encrypt(user+" | "+server+" | "+port+" | "+userServer+" | "+passwordServer, cle);
	 
	 bw.write(chaine);
	 bw.newLine();
	 bw.close();
	 
	 
	
}

/**
 * 
 * @param user
 * @param pass
 * @param dest
 * @return
 * @throws IOException
 */
public boolean envoyerFichierConf(String user, String pass,String dest) throws IOException {
	File file = new File("/"+dest+"/fichierConfig.txt");
	File inputFile = new File("serveursFTP.txt"); 
	if (file.createNewFile()) {
		
		FileWriter fw = new FileWriter(file.getAbsoluteFile(),true);
		BufferedWriter bw = new BufferedWriter(fw);
		
		
		BufferedReader lecteurAvecBuffer = null;
	    String ligne;
		lecteurAvecBuffer = new BufferedReader(new FileReader(inputFile));
		while ((ligne = lecteurAvecBuffer.readLine()) != null) {
			
			String chaine = decrypt(ligne,pass);
		
			if (chaine != null) {
			//	System.out.println(chaine.split(" | ")[0]);
				if(chaine.split(" | ")[0].equals(user)) { 
					bw.write(chaine);
					 bw.newLine();
				}
			}
		}
		lecteurAvecBuffer.close();
		bw.close();	
		return true;
	}
	return false;
}

/**
 * 
 * @param file
 * @throws IOException
 */
public void enregister_nom_fic(String file) throws IOException {
	this.ftp.cwd(file);
	this.ftp.pwd();
	this.ls();
	
	for (FTPFile ftp : this.ftp.listFiles(file)) {
		String name = ftp.getName();
		
		if(!name.equals("..") && !name.equals(".") && !name.equals(".DS_Store") && !name.equals(".doxyfile") && !name.equals("deleted__") ) {
			if(ftp.isDirectory()) {
				if(! FlopBox_Service.nomReperoire.contains(file+"/"+name))
					FlopBox_Service.nomReperoire.add(file+"/"+name);
				this.enregister_nom_fic(file+"/"+name);
				this.ftp.cwd("..");
			}
			else if(ftp.isFile()) {
				if(! FlopBox_Service.nomFic.contains(file+"/"+name)) {
					String lastModification = this.ftp.getModificationTime(file+"/"+name);
				    InoeudFichier inoeudFic = new InoeudFichier(file+"/"+name);
				    FlopBox_Service.nomFic.add(file+"/"+name);
			    	FlopBox_Service.lastModification.put(file+"/"+name, lastModification);
				}
			}
		}
	}
}

/**
 * 
 * @param chemin_locale
 * @param cheminServer
 * @throws IOException
 * @throws ParseException
 */
public void update_locale(String chemin_locale,String cheminServer) throws IOException, ParseException {
	
	this.ftp.cwd(cheminServer);
	this.ftp.pwd();
	if (cheminServer.equals("/")) cheminServer = "";
	this.ls();
	for (FTPFile ftp : this.ftp.listFiles(cheminServer)) {
		
		String name = ftp.getName();
		//System.out.println(cheminServer+"/"+name);
		if(!name.equals("..") && !name.equals(".") && !name.equals(".DS_Store") && !name.equals(".doxyfile") && !name.equals("deleted__") ) {
			if(ftp.isDirectory()) {
				 if(!FlopBox_Service.nomReperoire.contains(cheminServer+"/"+name)) {
      			   FlopBox_Service.nomReperoire.add(cheminServer+"/"+name);
      			   this.enregister_nom_fic(cheminServer+"/"+name);
      			   
      			   this.getR(cheminServer+"/"+name,chemin_locale);
      			   
      		     }
				 else {
					 this.update_locale(chemin_locale,cheminServer+"/"+name);
					 this.ftp.cwd("..");
				 }
				
			}
			else if (ftp.isFile()) {
				
				String lastModif = this.ftp.getModificationTime(cheminServer+"/"+name);
			    InoeudFichier inoeudFic = new InoeudFichier(cheminServer+"/"+name);
			    if(!FlopBox_Service.nomFic.contains(cheminServer+"/"+name)) {
			    	//System.out.println(cheminServer+"/"+name);
			    	FlopBox_Service.nomFic.add(cheminServer+"/"+name);
			    	FlopBox_Service.lastModification.put(cheminServer+"/"+name, lastModif);
			    	System.out.println("Chemin server : "+cheminServer+"/"+name);
					System.out.println("chemin locale : "+chemin_locale);
         	        this.get(cheminServer+"/"+name, chemin_locale, false);
			    	
			    }
			    
			    else {
			    	//System.out.println("oui");
			    	String lastModificationFile = FlopBox_Service.lastModification.get(cheminServer+"/"+name);
			    	
     			    if(!lastModificationFile.equals(lastModif)) {
     				  FlopBox_Service.lastModification.put(cheminServer+"/"+name,lastModif);
     				  this.get(cheminServer+"/"+name, chemin_locale, false);
     			   }
			    }
				
				
			}
		}
	}
	//System.out.println("-------------------------");
	
}



public void createDirectoryDelete() {
	
	try {
		this.ftp.makeDirectory("/deleted__");
		
	} catch (IOException e) {
		
		e.printStackTrace();
	}
}


/**
 * 
 * @param file
 * @throws IOException
 * @throws ParseException
 */
public String move(String file) throws IOException, ParseException {
	
	this.ftp.cwd(file);
	this.pwd();
	FlopBox_Service.nomReperoire.remove(file);
	FlopBox_Service.nomFic.remove(file);
	String chemin = this.ftp.getReplyString().split(" ")[0];
	String c = "";
	String[] ss = file.split("/");
	if(!chemin.equals("550")) {
		for(int i = 0;i < ss.length;i++) {
			this.ftp.cwd("..");
		}
	}
	
	for(int i = 0;i < ss.length-1;i++) {
		
		if(i==0) {
			c = ss[i]+"/";
			
			this.MKD("deleted__/");
			
		}
		else {
			c = c+"/"+ss[i];
			this.MKD("deleted__/");
		}
		
	}
	FTPFile[] remoteFiles = this.ftp.listFiles(file);
	
	//System.out.println(chemin+" kkkkkk   "+remoteFiles.length);
	if(chemin.equals("550")) {
		this.ftp.rename(file, "/deleted__");
		FlopBox_Service.nomFic.remove(file);
	}
	
	else {
		c = c+"/"+ss[ss.length-1];
		this.moveDirectory(file);
}
	//System.out.println(FlopBox_Service.nomReperoire);
	return this.ftp.getReplyString();
	
}

private void moveDirectory(String file) throws IOException, ParseException{
	this.MKD("/deleted__");
	for (FTPFile ftp : this.ftp.listFiles(file)) {
		String name = ftp.getName();
		if(!name.equals("..") && !name.equals(".") && !name.equals(".doxyfile") && !name.equals(".delete") && !name.equals(".DS_Store")) {
			if(ftp.isDirectory()) {
				FlopBox_Service.nomReperoire.remove(file);
				//FlopBox_Service.nomReperoire.remove(file+"/"+name);
				this.moveDirectory(file+"/"+name+"/");
				this.RMD(file+"/"+name);
				
			}
			else {
			
				this.ftp.rename(file, "/deleted__"+"/"+name);
				FlopBox_Service.nomFic.remove(file+"/"+name);
			}
		}
	}
	this.RMD(file);
	
}

/**
 * 
 * @return
 * @throws IOException
 */
public String showDeletedFiles() throws IOException {
	
	String rep=FlopBox_Service.HEAD;
	rep += show("/deleted__","");
	rep = rep +FlopBox_Service.FOOT;
	return rep;
}

/**
 * 
 * @param directory
 * @param car
 * @return
 * @throws IOException
 */
public String show(String directory,String car ) throws IOException {
	
	this.ftp.cwd(directory);
	this.ls();
	for (FTPFile ftp : this.ftp.listFiles(directory)) {
		String name = ftp.getName();
		
		if(!name.equals("..") && !name.equals(".") && !name.equals(".doxyfile") && !name.equals(".delete") && !name.equals(".DS_Store")) {
			
			FlopBox_Service.TEXT = FlopBox_Service.TEXT +"|-"+car+name+"<br/>";
			if(ftp.isDirectory()) {
				//blocquote = blocquote+car+"<li>"+name+"</li>";
				String car2 = car + "-";
				show(directory+"/"+name,car2);
				this.ftp.cwd("..");
				
			}
			
		}
	}
	
		return FlopBox_Service.TEXT ;

	
}

/**
 * 
 * @param path
 * @return
 * @throws IOException
 */
public String recupUnDossierSuprime(String path) throws IOException {
	
	this.ftp.rename("/deleted__/"+path,path);
	String rep = this.ftp.getReplyString();
	
	return rep;
}

/**
 * 
 * @param path
 * @return
 * @throws IOException
 */
public String suprimerDefinitifUnDossierSuprime(String path) throws IOException {
	this.RMD("/deleted__/"+path);
	return this.ftp.getReplyString();
	
}






}
