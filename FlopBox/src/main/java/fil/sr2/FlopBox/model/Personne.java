package fil.sr2.FlopBox.model;

public class Personne {
	
	private String motPourSupprimerUnServeur;
	private String nomServeur;
	private int portServeur;
	//private String userName;
	
	public Personne(String mot,String serveur,int port) {
		this.motPourSupprimerUnServeur = mot;
		this.nomServeur = serveur;
		this.portServeur = port;
		//this.userName = user;
	}

	public String getMotPourSupprimerUnServeur() {
		return motPourSupprimerUnServeur;
	}

	public void setMotPourSupprimerUnServeur(String motPourSupprimerUnServeur) {
		this.motPourSupprimerUnServeur = motPourSupprimerUnServeur;
	}

	public String getNomServeur() {
		return nomServeur;
	}

	public void setNomServeur(String nomServeur) {
		this.nomServeur = nomServeur;
	}

	public int getPortServeur() {
		return portServeur;
	}

	public void setPortServeur(int portServeur) {
		this.portServeur = portServeur;
	}

	

}