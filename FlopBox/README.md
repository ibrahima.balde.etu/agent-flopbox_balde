## Projet Application FLOP BOX
Thierno Amadou Bah
25/03/2021

### Introduction

Ce projet a pour but de mettre en place une plate-forme FlopBox en adoptant le style architectural REST pour permettre de centraliser la gestion des fichiers distants stockés dans des serveurs FTP.<br/>


Liste des commandes
-------------


Commandes    | Description    | Exemples
--------     | ------------  | ---
createAccount | pour creer un compte flop box <code> user : prof et mot de passe : 12345 </code> | curl -X POST -i 'http://localhost:8080/flopbox/compte/prof/12345'
conflopbox | pour se connecter sur flop box avec un <code> user : anonymous et mot de passe : anonymous </code>  | curl -X GET -i http://localhost:8080/flopbox/anonymous/anonymous
recupFicConf | pour recuperer les alias dans un fichier avec <code> user flop box : amadou et mot de passe : 1345 et chemin : le chemin complet où nous voulon garder le fichier </code> | curl -X GET -i 'http://localhost:8080/flopbox/fichierConfiguration/amadou/12345/chemin'
conflopbox | pour se connecter sur flop box avec un <code> user : anonymous et mot de passe : anonymous </code>  | curl -X GET -i http://localhost:8080/flopbox/anonymous/anonymous
enr | pour enregistrer un serveur dans flop box avec <code> ftp.ubuntu.com : le serveur, 21 : le port,user : user seveur , 123 : password serveur,anonymous : le mot de passe de flopbox et ab : le mot avec le quel on va supprimer le serveur dans flop box  </code> | curl -X POST -i 'http://localhost:8080/flopbox/ftp.ubuntu.com/21/user/123/anonymous/ab'
recupDocDel | pour recuperer un dossier supprimer avec <code> doc : le chemin complet du fichier doc dans  deleted, monServeur : le serveur, 2121 : le port,anonymous : le username et mot de passe du serveur   </code> | curl -X POST -i 'http://localhost:8080/flopbox/document/doc/monServeur/2121/anonymous/anonymous'
showDelFile | pour afficher les dossiers supprimés avec  <code> monServeur : le serveur, 2121 : le port,anonymous : le username et mot de passe du serveur  </code>| curl -X GET -i 'http://localhost:8080/flopbox/files/monServeur/2121/anonymous/anonymous
delDefiniti | pour supprimer definitivement un fichier dans le dossier deleted avec  <code> nomDoc:le chemin complet du fichier nomDoc dans  deleted , monServeur : le serveur, 2121 : le port,anonymous : le username et mot de passe du serveur  </code>| curl -X DELETE -i 'http://localhost:8080/flopbox/document/nomDoc/monServeur/2121/anonymous/anonymous
sup | pour supprimer un serveur dans le flop box avec <code> ftp.ubuntu.com : le serveur, ab : le mot avec lequel on a enregistrer le serveur dans flop box</code>  | curl -X DELETE -i 'http://localhost:8080/flopbox/ftp.ubuntu.com/ab'
list_servers | pour lister les serveurs stokés dans flop box | curl -X GET -i http://localhost:8080/flopbox/serveurs
con | pour se connecter a un serveur distant avec <code> ftp.ubuntu.com : le serveur, 21 : le port, anonymous : le user et anonymous : mot de passe </code> | curl -X GET -i 'http://localhost:8080/flopbox/ftp.ubuntu.com/21/anonymous/anonymous'
cwd | Changer de repertoire dans le serveur avec <code> cdimage : le nom de repertoir qu'on veut acceder,ftp.ubuntu.com : le serveur, 21 : le port, anonymous : le user et anonymous : mot de passe </code> | curl -X GET -i 'http://localhost:8080/flopbox/repertoires/cdimage/ftp.ubuntu.com/21/anonymous/anonymous'
putt | Stoker un fichier  text serveur avec <code> /home/lenovo/Images/if.txt le chemin du fichier, localhost : le serveur, 2121 : le port, user : le user et 12345 : mot de passe </code> | curl -X POST -i 'http://localhost:8080/flopbox/text/home/lenovo/Images/if.txt/localhost/2121/user/12345'
putb | Stoker un fichier  binaire serveur avec <code> /home/lenovo/Images/imge.png le chemin du fichier, localhost : le serveur, 2121 : le port, user : le user et 12345 : mot de passe </code> | curl -X POST -i 'http://localhost:8080/flopbox/binaire/home/lenovo/Images/imge.png/localhost/2121/user/12345'
mkd | pour creer un repertoire dans le serveur <code> aaa le nom du document, localhost : le serveur, 2121 : le port, user : le user et 12345 : mot de passe </code> | curl -X POST -i http://localhost:8080/flopbox/dossier/aaa/localhost/2121/user/12345
gett | afficher le contenu d'un fichier text serveur <code> README.md le nom du fichier, localhost : le serveur, 2121 : le port, user : le user et 12345 : mot de passe </code> | curl -X GET -i 'http://localhost:8080/flopbox/text/dossier/README.md/localhost/2121/user/12345'
getb | afficher le contenu d'un fichier text serveur  <code> image.png.md le nom du fichier, localhost : le serveur, 2121 : le port, user : le user et 12345 : mot de passe </code> | curl -X GET -i 'http://localhost:8080/flopbox/binaire/dossier/image.png/localhost/2121/user/12345'
list | liste un reprtoire  <code> cdimage le nom du repertoire, localhost : le serveur, 2121 : le port, user : le user et 12345 : mot de passe </code> | curl -X GET -i 'http://localhost:8080/flopbox/dossiers/cdimage/ftp.ubuntu.com/21/anonymous/anonymous'
port | port de connection <code> port: le numero du port, localhost : le serveur, 2121 : le port, user : le user et 12345 : mot de passe </code> | curl -X HEAD -i 'http://localhost:8080/flopbox/2121/ftp.ubuntu.com/21/anonymous/anonymous'
rnd | renommer un repertoire <code> essai2 le nom du repertoire qu'on veut renommer, '-' : le separateur de nom des deux fichiers, essai0 : le nouveau nom du fichier, localhost : le serveur, 2121 : le port, user : le user et 12345 : mot de passe </code> | curl -X PUT -i http://localhost:8080/flopbox/dossier/essai2/-/essai0/localhost/2121/user/12345
rnf | renommer un fichier <code> if.txt le nom du fichier qu'on veut renommer, '-' : le separateur de nom des deux fichiers, else.txt : le nouveau nom du fichier, localhost : le serveur, 2121 : le port, user : le user et 12345 : mot de passe </code> | curl -X PUT -i http://localhost:8080/flopbox/file/if.txt/-/else.txt/localhost/2121/user/12345
rmd | supprimer un dossier (et sous repertoire) dans le serveur  <code> esai0 le nom du repertoire, localhost : le serveur, 2121 : le port, user : le user et 12345 : mot de passe </code> | curl -X HEAD -i http://localhost:8080/flopbox/essai0/localhost/2121/user/12345
rmf | supprimer un dossier (et sous repertoire) dans le serveur  <code> esai0 le nom du repertoire, localhost : le serveur, 2121 : le port, user : le user et 12345 : mot de passe </code> | curl -X HEAD -i http://localhost:8080/flopbox/essai0/localhost/2121/user/12345
clos | ferme une connexion avec un client dans le serveur  <code>localhost : le serveur, 2121 : le port, user : le user et 12345 : mot de passe </code> | curl -X HEAD -i http://localhost:8080/flopbox/localhost/2121/user/12345



### Codes samples

- Se connecter sur le compte de FlopBox
-- Il faut renseigner le user et le mot de pass
  Si le user et mot de passe sont valides,l'utilisateur est alors connecté sur le flop box.

```
@GET
@Path("/conflopbox/{userFlopBox}/{passFlopBox}")
@Produces("text/html")
public String ConFlopBox(@PathParam("userFlopBox") String user,@PathParam("passFlopBox") String pass) {
 if(this.flopbox.authentificationFlopBox(user, pass)) {
   FlopBox_Resource.hasConnectedInFlopBox = true;
   return FlopBox_Service.HEAD+"<h3>Connexion reussie</h3>"+FlopBox_Service.FOOT;
 }
 return FlopBox_Service.HEAD+"<h3> user or mot de passe incorrect </h3>"+FlopBox_Service.FOOT;
}

```


### Compilation et execution du projet
La compilation et exécution du projet se font dans le répertoire <code>FlopBox/</code>

Pour compiler le projet il faut lancer la commande :<code> mvn package</code>
L'execution du projet se fait en lançant la commande :<code> mvn exec:java</code>


### Remarque
-- Il faut se connecter sur la plate forme flop box avant d'envoyer une requête. Au cas contraire,il faut creer un compte !

