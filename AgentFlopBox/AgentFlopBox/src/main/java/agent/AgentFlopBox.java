package agent;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.*;

/**
 * @author Ibrahima BALDE
 *
 */
public class AgentFlopBox /*implements Runnable*/{
	private String nomServerFTP;
	private String passwordFTP;
	private String userFTP;
	private int portFTP;
	private String userFlopBox;
	private String passwordFlopBox;
	private String  cheminRepertoirCourant;
	private static DirWatcher dirWatcher;
	public static  Map<String,String> inoeudFichier = new HashMap<>();
	
	public static  Map<String,String> lastModification = new HashMap<>();
	public static List<String> nomReperoire = new ArrayList<>();
	public static List<String> nomFic = new ArrayList<>();
	
	public static List<String> nomReperoire_tmp = new ArrayList<>();
	public static List<String> nomFic_tmp = new ArrayList<>();
	
	public static List<String> tmp = new ArrayList<>();
	public static List<String> tmp2 = new ArrayList<>();
	
	
	public static int START = 1;
	public static List<String> directoryDeleted = new ArrayList<>(); 
	
	
	
	/**
	 * Create an Agent Flop Box
	 * @param user the username of the compte flop box
	 * @param password the password of the compte flop box
	 * @param user the chemin the path where we want to put the content of the server
	 * @param password the userNameFTP the username of the server
	 * @param user the passFTP the password of the server
	 * @param password the port the port of the server
	 * @param serverFTP the name of the server
	 * 
	 */

	public AgentFlopBox(int j,String user,String password,String chemin,String userNameFTP,String passFTP,int port,String serverFTP) {
		this.userFlopBox = user;
		this.passwordFlopBox = password;
		this.cheminRepertoirCourant = chemin+"/"+serverFTP+j;
		this.userFTP = userNameFTP;
		this.passwordFTP = passFTP;
		this.portFTP = port;
		this.nomServerFTP = serverFTP;
		dirWatcher = new DirWatcher(this.cheminRepertoirCourant,"") {
			@Override
			protected void onChange(File file, String action) {

			}
		};
		Timer timer = new Timer();
		timer.schedule( dirWatcher , new Date(), 1000 );

	}

	/**
	 * Fonction qui permet de connecter la plateforme flopbox au serveur local ou distant
	 *@throws IOException
	 */
	public  void connected_flopbox_server() throws IOException {
		String connexion = "http://localhost:8080/flopbox/" + this.userFlopBox + "/" + this.passwordFlopBox;
		URL url = new URL(connexion);	
		BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
		connexion = "http://localhost:8080/flopbox/"+this.nomServerFTP+"/"+this.portFTP+"/"+this.userFTP+"/"+this.passwordFTP;
		url = new URL(connexion);	
		reader = new BufferedReader(new InputStreamReader(url.openStream()));

	}


	public void sendDataLocalToServer( String cheminLocal, String cheminServer ) throws IOException {

		System.out.println("liste des fichiers  "+ this.dirWatcher.getDir2());
		//System.out.println("chemin "+ this.cheminRepertoirCourant);
		for ( String filename : this.dirWatcher.getDir2()
			 ) {
			if (! AgentFlopBox.nomReperoire_tmp.contains(filename)) {
				String r2 = cheminLocal;
				String r = cheminLocal + "/" + filename;

				String chaine2 = "http://localhost:8080/flopbox/text" + cheminLocal + "/" + filename + "/-" + cheminServer + "/" + this.nomServerFTP + "/" + this.portFTP + "/" + this.userFTP + "/" + this.passwordFTP;
				System.out.println("chemin serveur : " + cheminServer);

				URL url = new URL(chaine2.trim());
				HttpURLConnection connection = (HttpURLConnection) url.openConnection();
				try {
					connection.setRequestMethod("POST");
				} catch (ProtocolException e) {
					e.printStackTrace();
				}
				BufferedReader reader2 = new BufferedReader(new InputStreamReader(connection.getInputStream()));

				reader2.readLine();
				AgentFlopBox.nomReperoire_tmp.add(filename);
			}
		}
		for ( String filename : this.dirWatcher.getDirFolder()
		) {
			if (! AgentFlopBox.nomReperoire_tmp.contains(filename)) {
				String r2 = cheminLocal;
				String r = cheminLocal + "/" + filename;
				String chaine2 = "http://localhost:8080/flopbox" + cheminLocal + "/" + filename + "/-" + cheminServer + "/" + this.nomServerFTP + "/" + this.portFTP + "/" + this.userFTP + "/" + this.passwordFTP;
				URL url = new URL(chaine2.trim());
				HttpURLConnection connection = (HttpURLConnection) url.openConnection();
				try {
					connection.setRequestMethod("POST");
				} catch (ProtocolException e) {
					e.printStackTrace();
				}
				BufferedReader reader2 = new BufferedReader(new InputStreamReader(connection.getInputStream()));
				reader2.readLine();
				AgentFlopBox.nomReperoire_tmp.add(filename);
			}


		}
		for ( String filename : this.dirWatcher.getDeletedFiles()
		) {

			String r2 = cheminLocal;
			String r = cheminLocal+"/"+filename;

			cheminServer = filename.substring(cheminServer.length()-1,filename.length());
			String chaine2 = "http://localhost:8080/flopbox/deleted_directory/"+cheminServer+"/"+this.nomServerFTP+"/"+this.portFTP+"/"+this.userFTP+"/"+this.passwordFTP;

			URL url = new URL(chaine2.trim());
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			try {
				connection.setRequestMethod("POST");
			}
			catch (ProtocolException e){
				e.printStackTrace();
			}
			BufferedReader	reader2 = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			reader2.readLine();
		}
	}

	

	/**
	 * Cette fonction nous permet de rajouter un nouveau dossier dans notre local si un dossier est ajouter dans le serveur
	* @throws IOException
	*/
	public void sendDataServerToLocal() throws IOException {
		int i = 0;
		if (this.cheminRepertoirCourant.charAt(0) == '/') {
			i = 1;
		}
		URL url = new URL("http://localhost:8080/flopbox/mise_a_jour/"+this.cheminRepertoirCourant+"/"+this.nomServerFTP+"/"+this.portFTP+"/"+this.userFTP+"/"+this.passwordFTP);
		BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
		reader.readLine();
		
	}

	/**
	 *@return the server's name
	 */
	public String getNomServeur() {
		return this.nomServerFTP;
	}


	/**
	 *@return the user's name of flop box
	 */
	public String getUserFlopBox() {
		return userFlopBox;
	}

	/**
	 *@param userFlopBox the user's name of the flop box
	 */
	public void setUserFlopBox(String userFlopBox) {
		this.userFlopBox = userFlopBox;
	}

	/**
	 *@return the user's password of the flop box
	 */
	public String getPasswordFlopBox() {
		return passwordFlopBox;
	}

	/**
	 *@param passwordFlopBox the user's password of the flop box
	 */
	public void setPasswordFlopBox(String passwordFlopBox) {
		this.passwordFlopBox = passwordFlopBox;
	}

	/**
	 *@return the current path
	 */
	public String getCheminRepertoirCourant() {
		return cheminRepertoirCourant;
	}

	/**
	 *@param cheminRepertoirCourant the current path
	 */

	public void setCheminRepertoirCourant(String cheminRepertoirCourant) {
		this.cheminRepertoirCourant = cheminRepertoirCourant;
	}


}

