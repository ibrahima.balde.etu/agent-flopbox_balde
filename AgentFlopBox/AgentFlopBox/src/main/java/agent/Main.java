package agent;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class Main{


	public static void main(String[] args) throws IOException, InterruptedException {

		if (args.length < 3){
			System.out.println("Usage : userFlopBox,passwordFlopBox,local folder");
			System.out.println("Example : amadou, 12345, /home/users/etudiant/Desktop/LocalFolder ");
		}
		try {
			assert(args.length == 3);
		}
		catch (Exception e){
			System.out.println("Usage : userFlopBox,passwordFlopBox,local folder");
			System.out.println("Example : amadou, 12345, /home/users/etudiant/Desktop/LocalFolder ");
		}
		String localFolder =  args[2];
		//"/home/users/etudiant/Desktop/LocalFolder"; //
		String cheminServer = "/";
			
		if (localFolder.charAt(localFolder.length()-1) == '/')
			localFolder = localFolder.substring(0,localFolder.length()-1);

		// Synchronisation avec les serveurs
		List<AgentFlopBox> servers = listServers(localFolder, args[0], args[1]);
		//System.out.println("servers "+servers);
		while (true) {
			for(AgentFlopBox  server : servers) {
				server.sendDataLocalToServer( server.getCheminRepertoirCourant(), cheminServer);
				server.sendDataServerToLocal();
			}
			
			Thread.sleep(10000);	
		}
	}

	/**
	 * Fonction qui permet de connecter de connecter les serveurs distants au local.
	 * @param localFolder
	 * @param args_0
	 * @param args_1
	 * @return
	 * @throws IOException
	 */
	public static List<AgentFlopBox> listServers(String localFolder, String args_0, String args_1) throws IOException {
		AgentFlopBox server = null;

		List<AgentFlopBox> servers = new ArrayList<>();
		int cptServer = 1;

		String line;
		BufferedReader serverData = new BufferedReader(new FileReader("fichierConfig.txt"));
		while ((line = serverData.readLine()) != null && !line.equals("")) {
			//cptServer++;
			String[] ss = line.split(" | ");

			File dossier = new File(localFolder+"/"+ss[2] + cptServer );
			dossier.mkdir();

			server = new AgentFlopBox( cptServer , args_0, args_1, localFolder, ss[6],ss[8],Integer.parseInt(ss[4]),ss[2]);
			servers.add(server);
			server.connected_flopbox_server();

			cptServer++;
		}
		serverData.close();

		return servers;
	}

}

