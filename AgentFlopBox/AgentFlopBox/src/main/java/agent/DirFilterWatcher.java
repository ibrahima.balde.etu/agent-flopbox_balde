package agent;

import java.io.*;

/**
 * Classe qui permet de filtrer le type de fichier/dossier qu'on veut modifier/ajouter ou supprimer
 */
public class DirFilterWatcher implements FileFilter {
    private String filter;

    public DirFilterWatcher() {
        this.filter = "";
    }

    public DirFilterWatcher(String filter) {
        this.filter = filter;
    }

    public boolean accept(File file) {
        if ("".equals(filter)) {
            return true;
        }
        return (file.getName().endsWith(filter));
    }
}
