package agent;

import java.util.*;
import java.io.*;

/**
 * Classe qui permet de detecter la modification, suppression et ajout d'un fichier/dossier
 */
public abstract class DirWatcher extends TimerTask {
    private String path;
    private File filesArray [];
    private HashMap dir = new HashMap();
    private DirFilterWatcher dfw;
    private List<String> deletedFiles;
    private static List<String> dir2; // list of files
    private static List<String> dirFolder; // list of folder.

    /**
     * Constructeur sans filtre
     * @param path
     */
    public DirWatcher(String path) {
        this(path, "");
        this.deletedFiles = new ArrayList<>();
        dir2 = new ArrayList<>();
        dirFolder = new ArrayList<>();
    }

    /**
     * Constructeur avec filtre
     * @param path
     * @param filter
     */
    public DirWatcher(String path, String filter) {
        this.path = path;
        dfw = new DirFilterWatcher(filter);
        filesArray = new File(path).listFiles(dfw);
        this.deletedFiles = new ArrayList<>();
        dir2 = new ArrayList<>();
        dirFolder = new ArrayList<>();
        // transfer to the hashmap be used a reference and keep the
        // lastModfied value
        for(int i = 0; i < filesArray.length; i++) {
            dir.put(filesArray[i], (filesArray[i].lastModified()));
            if (filesArray[i].isDirectory()){
                dirFolder.add(filesArray[i].getName());
            }
            else {
                dir2.add(filesArray[i].getName());
            }


        }
    }

    public final void run() {
        HashSet checkedFiles = new HashSet();
        filesArray = new File(path).listFiles(dfw);

        // scan the files and check for modification/addition
        for(int i = 0; i < filesArray.length; i++) {
            Long current = (Long)dir.get(filesArray[i]);
            checkedFiles.add(filesArray[i]);
            if (current == null) {
                // new file
                dir.put(filesArray[i], (filesArray[i].lastModified()));
                if (filesArray[i].isDirectory()){
                    dirFolder.add(filesArray[i].getName());
                }else {
                    dir2.add(filesArray[i].getName());
                }
                onChange(filesArray[i], "add");
            }
            else if (current.longValue() != filesArray[i].lastModified()){
                // modified file
                dir.put(filesArray[i], filesArray[i].lastModified());
                if (filesArray[i].isDirectory()){
                    dirFolder.add(filesArray[i].getName());
                }else {
                    dir2.add(filesArray[i].getName());
                }
                onChange(filesArray[i], "modify");
            }
        }

        // now check for deleted files
        Set ref = ((HashMap)dir.clone()).keySet();
        ref.removeAll((Set)checkedFiles);

        Iterator it = ref.iterator();
        while (it.hasNext()) {
            File deletedFile = (File)it.next();
            this.deletedFiles.add(deletedFile.getName());
            dir.remove(deletedFile);
            if (deletedFile.isDirectory()){
                dirFolder.remove(deletedFile.getName());
            }else {
                dir2.remove(deletedFile.getName());
            }
            onChange(deletedFile, "delete");
        }
    }


    protected abstract void onChange( File file, String action );

    public String getPath() {
        return path;
    }

    public File[] getFilesArray() {
        return filesArray;
    }

    public HashMap getDir() {
        return dir;
    }

    public DirFilterWatcher getDfw() {
        return dfw;
    }

    public List<String> getDeletedFiles() {
        return deletedFiles;
    }

    public List<String> getDir2() {
        return dir2;
    }

    public static List<String> getDirFolder() {
        return dirFolder;
    }

    public static void main(String[] args) {
        TimerTask folder = new DirWatcher("C:\\Users\\ibrah\\Desktop\\TestSR\\Ubuntu") {
            @Override
            protected void onChange(File file, String action) {
                System.out.println("file "+ file.getName() + " "+ action);
            }
        };
        Timer timer = new Timer();
        timer.schedule( folder , new Date(), 1000 );
    }
}
