## Projet nº2 - Systèmes Répartis 2 : Agent FlopBox

## Auteur

- Ibrahima BALDE

## Encadrant

- Arthur d'Azemar

30/04/2021

## Introduction

Développement d'une application cliente pour la plate-forme FlopBox qui permette de synchroniser les données stockées à distance dans un ensemble de serveurs FTP avec le système de fichiers local d'une machine sur laquelle l'application cliente sera exécutée.

Pour stocker les comptes utlisateurs dans la plateforme, un fichier a été utilisé .

Le code a été fait en java et j'ai utilisé l'api flopbox de Thierno Amadou Bah G1 Genie informatique que j'ai modifié.

### Architecture



- La classe AgentFlopBox : a trois principales méthodes.
 - Connecter la plateforme flopbox et le serveur distant : connected_flopbox_server
 - Envoyer les données du local au serveur pour mettre a jour le serveur : sendDataLocalToServer
 - Envoyer les données du serveur au local pour mettre a jour le local : sendDataServerToLocal

- La classe DirWatcher : qui permet de detecter la modification, suppression et ajout d'un fichier/dossier
- La classe DirWatcherFilter : qui permet de filtrer le type de fichier/dossier qu'on veut modifier/ajouter ou supprimer


### Codes samples

- Petit bout de code qui permet d'envoyer un fichier binaire/text du local au serveur. Il parcourt la liste des fichiers
qui se trouvent dans dossier local lié au serveur.

```
   public void sendDataLocalToServer( String cheminLocal, String cheminServer ) throws IOException {
        ......
		for ( String filename : this.dirWatcher.getDir2()
			 ) {
			if (! AgentFlopBox.nomReperoire_tmp.contains(filename)) {
				String r2 = cheminLocal;
				String r = cheminLocal + "/" + filename;

				String chaine2 = "http://localhost:8080/flopbox/text" + cheminLocal + "/" + filename + "/-" + cheminServer + "/" + this.nomServerFTP + "/" + this.portFTP + "/" + this.userFTP + "/" + this.passwordFTP;
				System.out.println("chemin serveur : " + cheminServer);

				URL url = new URL(chaine2.trim());
				HttpURLConnection connection = (HttpURLConnection) url.openConnection();
				try {
					connection.setRequestMethod("POST");
				} catch (ProtocolException e) {
					e.printStackTrace();
				}
				BufferedReader reader2 = new BufferedReader(new InputStreamReader(connection.getInputStream()));

				reader2.readLine();
				AgentFlopBox.nomReperoire_tmp.add(filename);
			}
		}
    ......
```

- Constructeur qui permet de detecter les dossiers ou fichiers ( de type filter) qui ont été modifiés

```
public DirWatcher(String path, String filter) {
        this.path = path;
        dfw = new DirFilterWatcher(filter);
        filesArray = new File(path).listFiles(dfw);
        this.deletedFiles = new ArrayList<>();
        dir2 = new ArrayList<>();
        dirFolder = new ArrayList<>();
        // transfer to the hashmap be used a reference and keep the
        // lastModfied value
        for(int i = 0; i < filesArray.length; i++) {
            dir.put(filesArray[i], (filesArray[i].lastModified()));
            if (filesArray[i].isDirectory()){
                dirFolder.add(filesArray[i].getName());
            }
            else {
                dir2.add(filesArray[i].getName());
            }


        }
    }

```
### Compilation et execution du projet
La compilation et exécution du projet se font dans le répertoire <code>AgentFlopBox/</code>.

Se trouver dans le repertoire FlopBox et Lancer l'api flopbox <code>FlopBox/</code> avec ***mvn exec:java***.

Pour compiler le projet il faut lancer la commande :<code> mvn package</code>
L'execution du projet se fait en lançant la commande :<code> java -jar target/agentflopbox-1.0-SNAPSHOT.jar userFlopBox passWordFlopBox CheminRepPourStokerFichier</code>

Exemple :

```bash
java -jar target/agentflopbox-1.0-SNAPSHOT.jar amadou 12345 /me/users/etudiant/Desktop/LocalFolder

```
NB : Veuillez utiliser user: amadou, mdp:12345


### Remaques :

1) Dès qu'un nouveau document est rajouté,il faut attendre 10s pour voir le resultat

2) La synchronisation ne marche pas bien en effet transferer les fichiers du serveur au local ne marche que pour les fichiers textes.
Donc par conséquent s'il transfert des images il les transforme en fichier texte.
3) La suppression ne marche pas très bien dans le cadre où on voudrait supprimer plusieurs fichiers ou dossiers.


### Demo 

# Pour les commandes,referez vous sur le readme de FlopBox

pour ce demo,on supose que nous avons un serveur python deja installer sur notre machine qui a comme {nom : localhost,port :2121,nomUser : balde,password : 12345}

- Telecharger les deux depôt sur git (FlopBox et AgentFlopBox)
1)


- rentrez dans le dossier FlopBox/
- compiler le projet FlopBox : <code> mvn package </code>
- executer le projet FlopBox: <code> mvn exec:java </code>
- ouvrez un autre terminale pour creer un compte FlopBox :</code>  curl -X POST -i 'http://localhost:8080/flopbox/	
    compte/prof/12345'  </code>
- Connectez-vous avec vos identifiants  puis  ajouter un serveur : <code> curl -X GET -i http://localhost:8080/flopbox/prof/12345</code>
   <code> curl -X POST -i 'http://localhost:8080/flopbox/localhost/2121/user/123/12345/ab'</code>
- recuperer le fichier de configuration  dans le dossier /agentflopbox/flopbox/agent_flop_box_bah/AgentFlopBox/agentflopbox en donnant le chemin complet :
  <code> curl -X GET -i 'http://localhost:8080/flopbox/fichierConfiguration/amadou/12345/chemin complet ... /AgentFlopBox/agentflopbox'</code>

2)

- ouvrez un autre terminale pour compiler le projet Agent FlopBox, : <code> mvn package </code>	
- rentrez dans le dossier agentflopbox/
- compiler le projet Agent Flopbox  : <code> mvn package </code>
- executer le projet Agent FlopBox : <code> java -jar target/agentflopbox-1.0-SNAPSHOT.jar prof 12345 /home/lenovo/Documents/locale </code>
- ajouter des dossiers en local ou dans le serveur.
- la mise à jour,c'est a chaque 10s

3) Pour supprimer definitivement les fichiers supprimés :
 
- par exemple le repertoire rep1 est supprimé en local,est il se trouve dans le dossier deleted dans le serveur, pour le suprimer deffinitifement :
  <code> curl -X DELETE -i 'http://localhost:8080/flopbox/document/rep1/localhost/2121/user/123'</code>
 



4) Pour recuperer les fichiers supprimés :
- par exemple le repertoire rep1 est supprimé en local,est il se trouve dans le dossier deleted dans le serveur, pour le recuperer :
<code> curl -X PUT -i 'http://localhost:8080/flopbox/document/rep1/localhost/2121/user/12345'</code>


### code pour creer un serveur en python
```
from pyftpdlib.authorizers import DummyAuthorizer
from pyftpdlib.handlers import FTPHandler
from pyftpdlib.servers import FTPServer
authorizer = DummyAuthorizer()
authorizer.add_user("user", "12345", repCourantpourStokerDoc, perm="elradfmwMT")
handler = FTPHandler
handler.authorizer = authorizer
server = FTPServer(("127.0.0.1", 2121), handler)
server.serve_forever()
```
